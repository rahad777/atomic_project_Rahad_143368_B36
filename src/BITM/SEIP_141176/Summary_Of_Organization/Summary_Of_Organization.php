<?php
namespace App\Summary_Of_Organization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
use PDO;

class Summary_Of_Organization extends DB
{
    public $id;
    public $org_Name;
    public $org_Summary;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("org_name", $postVariabledata)) {
            $this->org_Name = $postVariabledata['org_name'];
        }
        if (array_key_exists("org_summary", $postVariabledata)) {
            $this->org_Summary = $postVariabledata['org_summary'];
        }

    }
    public function store(){
        $arrData=array($this->org_Name,$this->org_Summary);
        $sql="insert into organization_summary(Org_Name,Org_Summary)VALUES
            (?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result)
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
    public function index($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from organization_summary WHERE is_deleted=0 ORDER BY id DESC ');


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
            
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;

    }// end of index()
    public function view($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from organization_summary where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()
    public function update(){
        $arrData  = array($this->org_Name,$this->org_Summary);

        $sql = 'UPDATE organization_summary  SET Org_Name = ?   , Org_Summary = ? where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('create.php');

    }//end of update
    public function delete(){

        $sql = "Delete from organization_summary where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('create.php');

    }// end of delete()


    public function trash(){

        $sql = "Update organization_summary SET is_deleted=1 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('create.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from organization_summary where is_deleted=1 ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update organization_summary SET is_deleted=0 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('create.php');

    }// end of recover();
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from organization_summary  WHERE is_deleted =0 LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();



    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from organization_summary  WHERE is_deleted=1  LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();
    public function search($requestArray){
        $sql = "";
        // if( isset($requestArray['byUser']) && isset($requestArray['byCity']) )  $sql = "SELECT * FROM `city` WHERE `is_deleted` =0 AND (`username` LIKE '%".$requestArray['search']."%' OR `city_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName'])) $sql = "SELECT * FROM `organization_summary` WHERE `is_deleted` =0 AND `Org_Name` LIKE '%".$requestArray['search']."%'";
        // if(!isset($requestArray['byUser']) && isset($requestArray['byCity']) )  $sql = "SELECT * FROM `city` WHERE `is_deleted` =0 AND `city_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `organization_summary` WHERE `is_deleted` =0";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->Org_Name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->Org_Name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end



        /*
                // for each search field block start
                $STH = $this->DBH->query($sql);
                $STH->setFetchMode(PDO::FETCH_OBJ);
                $allData= $STH->fetchAll();
                foreach ($allData as $oneData) {
                    $_allKeywords[] = trim($oneData->city_name);
                }
                $STH = $this->DBH->query($sql);
                $STH->setFetchMode(PDO::FETCH_OBJ);
                $allData= $STH->fetchAll();
                foreach ($allData as $oneData) {

                    $eachString= strip_tags($oneData->city_name);
                    $eachString=trim( $eachString);
                    $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                    $eachString= str_replace("&nbsp;","",  $eachString);
                    $WordsArr = explode(" ", $eachString);

                    foreach ($WordsArr as $eachWord){
                        $_allKeywords[] = trim($eachWord);
                    }
                }
                // for each search field block end
        */

        return array_unique($_allKeywords);


    }// get all keywords




}




