$(document).ready(function() {

    // Generate a simple captcha
    $('#summaryform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
           org_name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The organization name is required and can\'t be empty'
                    },
                    stringLength: {

                        min: 2,

                        max: 30,

                        message: 'The username must be more than 2 and less than 30 characters long'

                    }
                }
            },
            org_summary: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The summary is required and can\'t be empty'
                    }
                }
            }
        }
    });
});
$(document).ready(function(){
    setTimeout(function(){ $('#message').fadeOut() }, 5000);
});