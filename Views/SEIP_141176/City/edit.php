<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\City\City;
if(!isset($_SESSION))session_start();

$objCity=new City();
$objCity->setData($_GET);
$oneData=$objCity->view("obj");

?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../JS/create.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>

</head>
<body>

    <form class="navbar-form navbar-right">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div class="row">
        <div class="page-header" id="message">
            <?php echo Message::message(); ?>
        </div></div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li ><a href="../BookTitle/create.php">Book Title</a></li>
                    <li><a href="../Birthdate/create.php">Birthday</a></li>
                    <li class="active"><a href="create.php">City</a></li>
                    <li><a href="../Email/create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li><a href="../Profile_Picture/create.php">Profile Picture</a></li>
                    <li><a href="../Summary_Of_Organization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-info" onclick="window.location.href='create.php'" >
                            <span class="glyphicon glyphicon-backward">Back</span></button>

                        <div class="nav navbar-right">
                            <label >booklist per page</label>
                            <select class="bg-primary">
                                <option id="select">5</option>
                                <option id="select">10</option>
                                <option id="select">15</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="well " id="navbar">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Edit_BookTitle<i class="fa fa-book"></i></h3>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form action="update.php" id="cityform" method="POST" role="form">
                                <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
                                <div class="form-group">
                                    <label for="usesrname">User Name</label>
                                    <input type="text" class="form-control"name="username"
                                           id="user" value="<?php echo $oneData->username?>"/>
                                </div>
                                <div class="form-group">
                                    <label for="city_name">City Name</label>
                                    <select class="form-control " name="cityname" id="cityName">
                                        <option value="Chittagong"<?php if($oneData->city_name=='Chittagong')echo 'selected';?>>Chittagong</option>
                                        <option value="Dhaka" <?php if($oneData->city_name=='Dhaka')echo 'selected';?>>Dhaka</option>
                                        <option value="Rajshahi" <?php if($oneData->city_name=='Rajshahi')echo 'selected';?>>Rajshahi</option>
                                        <option value="Shylet" <?php if($oneData->city_name=='Shylet')echo 'selected';?>>Shylet</option>
                                        <option value="Khulna" <?php if($oneData->city_name=='Khulna')echo 'selected';?>>Khulna</option>
                                        <option value="Barishal" <?php if($oneData->city_name=='Barishal')echo 'selected';?>>Barishal</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info" ">update </button>



                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>



</body>

</html>