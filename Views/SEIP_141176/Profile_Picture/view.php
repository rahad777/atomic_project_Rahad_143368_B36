<?php
require_once("../../../vendor/autoload.php");

use App\Profile_Picture\Profile_Picture;

$objIMAGE  =  new Profile_Picture();
$objIMAGE->setData($_GET);
$oneData= $objIMAGE->view("obj");


?>
<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../JS/create.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>

</head>
<body>
<div class="container" id="contain">

    <div class="jumbotron" id="picture">
        <h2></h2>


    </div>

    <form class="navbar-form navbar-right">
    </form>
    <div class="row">
        <div class="page-header" id="Mmessage">

        </div></div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li ><a href="../BookTitle/create.php">Book Title</a></li>
                    <li><a href="../Birthdate/create.php">Birthday</a></li>
                    <li><a href="../City/create.php">City</a></li>
                    <li><a href="../Email/create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li class="active"><a href="create.php">Profile Picture</a></li>
                    <li ><a href="../Summary_Of_Organization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-info" onclick="window.location.href='create.php'" >
                            <span class="glyphicon glyphicon-backward">&nbsp;Back</span></button>

                        <div class="nav navbar-right">

                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="well " id="navbar">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Edit_Organization Summary<i class="fa fa-book"></i></h3>
                            </div>
                        </div>
                        <table class="table table-responsive">

                            <thead>
                            <tr class="bg-primary">
                                <th>User Name</th>
                                <th>Profile Picture</th>


                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo $oneData->username;?></td>
                                <td><img src="<?php echo "upload/".$oneData->image_url;?>"alt="image" height="300px" width="300px"></td>
                            </tr>
                            </tbody>


                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>


</div>



</body>

</html>