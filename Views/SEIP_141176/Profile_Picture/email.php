<?php
session_start();
include_once('../../../vendor/autoload.php');
require '../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

use App\Profile_Picture\
use App\Utility\Utility;
use App\Message\Message;


$profilePicture = new Profile_Picture();

if(isset($_REQUEST['list'])) {
    $list = 1;
    $recordSet = $profilePicture->index();

}
else {
    $list= 0;
    $profilePicture->prepare($_REQUEST);
    $singleItem = $profilePicture->view();
}

?>



<!DOCTYPE html>

<head>
    <title>Email This To A Friend</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({
            selector: 'textarea',  // change this value according to your HTML

            menu: {
                table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
                tools: {title: 'Tools', items: 'spellchecker code'}

            }
        });
    </script>


</head>

<body>


<div class="container">
    <h2>Email This To A Friend</h2>
    <form  role="form" method="post" action="email.php<?php if(isset($_REQUEST['id'])) echo "?id=".$_REQUEST['id']; else echo "?list=1";?>">
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="text"  name="name"  class="form-control" id="name" placeholder="Enter name of the recipient ">
            <label for="Email">Email Address:</label>
            <input type="text"  name="email"  class="form-control" id="email" <?php if(!$list) echo "value=\"". $singleItem->student_email; else echo 'placeholder="Enter recipient email address here..."'; ?>">

            <label for="Subject">Subject:</label>
            <input type="text"  name="subject"  class="form-control" id="subject" value="<?php if($list){echo "List of Profile Photos recommendation";} else {echo "A single Profile Photo recommendation";} ?>">
            <label for="body">Body:</label>
            <textarea   rows="10" cols="160"  name="body" >
<?php
if($list){

    $trs="";
    $sl=0;

    printf("<table><tr>Active List of Profile Pictures</tr><tr><td>Serial</td><td></td><td>ID</td><td>Name</td><td>Profile Picture</td>");

    while($row = $recordSet->fetch_assoc()) {

        $id = $row['id'];

        $student_name = $row['student_name'];

        $student_photo = $row['student_photo'];

        $sl++;

        printf("<tr><td>%d</td><td></td><td>%d</td><td>%s</td><td><img src=\"../../../resource/images/ProfilePictures/%s\" alt=\"image\" height=\"100px\" width=\"100px\" class=\"img - responsive\"></td></tr>",$sl,$id,$student_name,$student_photo);


    }


}
else
{

    printf("I'm recommending You:<br>  Profile ID:%s,<br> Profile Name:%s,<br> Profile Picture: <br> <img src=\"../../../resource/images/ProfilePictures/%s\" alt=\"image\" height=\"100px\" width=\"100px\" class=\"img - responsive\">",$singleItem->id,$singleItem->student_name,$singleItem->student_photo);

}
?>
            </textarea>

        </div>

        <input class="btn-lg btn-primary" type="submit" value="Send Email">

    </form>


    <?php



    if(isset($_REQUEST['email'])&&isset($_REQUEST['subject'])) {
        date_default_timezone_set('Etc/UTC');

        //Create a new PHPMailer instance
        $mail = new PHPMailer;
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 2;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';
        // use
        // $mail->Host = gethostbyname('smtp.gmail.com');
        // if your network does not support SMTP over IPv6
        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587; //587
        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = 'tls'; //tls
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "bitm.php22@gmail.com";
        //Password to use for SMTP authentication
        $mail->Password = "seip1567";
        //Set who the message is to be sent from
        $mail->setFrom('bitm.php22@gmail.com', 'BITM PHP - 22');
        //Set an alternative reply-to address
        $mail->addReplyTo('bitm.php22@gmail.com', 'BITM PHP - 22');
        //Set who the message is to be sent to
        $mail->addAddress($_REQUEST['email'], $_REQUEST['name']);
        //Set the subject line
        $mail->Subject = $_REQUEST['subject'];

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body

        $fp = fopen('photo.html','w+');
        fprintf($fp,"%s",$_REQUEST['body']);
        fclose($fp);
        $mail->msgHTML(file_get_contents('photo.html'), dirname(__FILE__));
        //Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';



        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            Message::message("
    <div class=\"alert alert-success\">
    <strong>Success!</strong> Email has been sent successfully.
</div>");
            //Utility::redirect("index.php");


            ?>
            <script type="text/javascript">
                window.location.href = 'index.php';
            </script>
            <?php


        }

    }


    ?>







</div>
</body>


</html>