<?php
require_once("../../../vendor/autoload.php");
use App\Profile_Picture\Profile_Picture;
if (isset($_FILES['image'])) {
    $errors = array();
    $name = time() . $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
    $format = array("jpeg", "jpg", "png");
    if (in_array($file_ext, $format) == false) {
        $errors[] = "extension not allowed,please choose a jpeg or png file";
    }
    if ($file_size > 2097152) {
        $errors[] = "file size must be less then 2 mb";
    }
    move_uploaded_file($tmp, 'upload/' . $name);


    $_POST['image'] = $name;
}
$objimage=new Profile_Picture();
$objimage->setData($_POST);
$objimage->update();