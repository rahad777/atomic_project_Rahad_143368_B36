<?php
include_once ('../../../vendor/autoload.php');
use App\Summary_Of_Organization\Summary_Of_Organization;
 $obj= new Summary_Of_Organization();
 $recordSet=$obj->index();
 //var_dump($allData);
$trs="";
$sl=0;




    foreach($recordSet as $row) {
        $id =  $row["id"];
        $orgName = $row['Org_Name'];
        $orgSummary = $row['Org_Summary'];

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='150'> $sl</td>";
        $trs .= "<td width='150'> $id </td>";
        $trs .= "<td width='300'> $orgName </td>";
        $trs .= "<td width='300'> $orgSummary </td>";


        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Organization Name</th>
                    <th align='left' >Organization Summary</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>




BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');