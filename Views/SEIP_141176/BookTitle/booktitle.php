
<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
echo Message::message();
?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>

    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>

</head>
<body>
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalNorm">
    Launch Normal Form
</button>

<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3>Please enter Book Title and Author Name</h3>
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form action="store.php" method="post" role="form" id="defaultForm">
                    <div class="form-group">
                        <label for="booktitle">Book Title</label>
                        <input type="text" class="form-control"name="booktitle"
                               id="booktitle" placeholder="Enter booktitle"/>
                    </div>
                    <div class="form-group">
                        <label for="author_name">Author Name</label>
                        <input type="text" class="form-control"name="author_name"
                               id="author_name" placeholder="Enter author_name"/>
                    </div>
                    <button type="submit" class="btn btn-info">Submit</button>


                </form>
            </div>
            <script type="text/javascript">

                $(document).ready(function() {

                    // Generate a simple captcha
                    $('#defaultForm').bootstrapValidator({

                        message: 'This value is not valid',

                    fields: {
                        booktitle: {

                            message: 'The username is not valid',

                            validators: {

                                notEmpty: {

                                    message: 'The username is required and can\'t be empty'

                                },

                                stringLength: {

                                    min: 6,

                                    max: 30,

                                    message: 'The username must be more than 6 and less than 30 characters long'

                                },

                                regexp: {

                                    regexp: /^[a-zA-Z_\.]+$/,

                                    message: 'The username can only consist of alphabetical, number, dot and underscore'

                                },

                                different: {

                                    field: 'password',

                                    message: 'The username and password can\'t be the same as each other'

                                }

                            }

                        },

                        email: {

                            validators: {

                                notEmpty: {

                                    message: 'The email address is required and can\'t be empty'

                                },

                                emailAddress: {

                                    message: 'The input is not a valid email address'

                                }

                            }

                        },

                        password: {

                            validators: {

                                notEmpty: {

                                    message: 'The password is required and can\'t be empty'

                                },

                                identical: {

                                    field: 'confirmPassword',

                                    message: 'The password and its confirm are not the same'

                                },

                                different: {

                                    field: 'username',

                                    message: 'The password can\'t be the same as username'

                                }

                            }

                        },

                        confirmPassword: {

                            validators: {

                                notEmpty: {

                                    message: 'The confirm password is required and can\'t be empty'

                                },

                                identical: {

                                    field: 'password',

                                    message: 'The password and its confirm are not the same'

                                },

                                different: {

                                    field: 'username',

                                    message: 'The password can\'t be the same as username'

                                }

                            }

                        }
                    }




                });
                });

            </script>
            <!-- Modal Footer -->
