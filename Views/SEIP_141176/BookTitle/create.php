<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\BookTitle\BookTitle;
$serial=1;
$objBOOKtitle=new BookTitle();
$alldata=$objBOOKtitle->index("obj");
$objBOOKtitle->index("obj");
################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $objBOOKtitle->search($_REQUEST);
$availableKeywords=$objBOOKtitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

$recordCount= count($alldata);
if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBOOKtitle->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objBOOKtitle->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################
?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/css/jquery-ui.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../bootstrap/js/jquery-ui.js"></script>
    <script src="../../../JS/create.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>
    
</head>
<body>

    <form class="navbar-form navbar-right">
        <div class="form-group" id="searchForm" action="create.php"  method="get">
            <input type="text" class="form-control" placeholder="Search" id="searchID" name="search"<span class="glyphicon glyphicon-search"></span>
        </div>
        <input type="checkbox"  name="byTitle"   checked  >By Title
        <input type="checkbox"  name="byAuthor"  checked >By Author
        <input hidden type="submit" class="btn-primary" value="search">
    </form>
    <div class="row">
    <div class="page-header" id="message">
<?php echo Message::message(); ?>
    </div></div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li class="active"><a href="create.php">Book Title</a></li>
                    <li><a href="../Birthdate/create.php">Birthday</a></li>
                    <li><a href="../City/create.php">City</a></li>
                    <li><a href="../Email/create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li><a href="../Profile_Picture/create.php">Profile Picture</a></li>
                    <li><a href="../Summary_Of_Organization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalNorm">
                            <span class="glyphicon glyphicon-plus">create new</span></button>
                        <a href="trashed.php"> <button type="button" class="btn btn-info"  >
                                <span class="glyphicon glyphicon-trash">Trashed List</span></button></a>
                        <a href="pdf.php" class="btn btn-info" role="button"><span class="glyphicon glyphicon-download">Download as PDF</span></a>
                        <a href="xl.php" class="btn btn-info" role="button"><span class="glyphicon glyphicon-download">Download as XL</span></a>

                        <div class="nav navbar-right">
                            <label >booklist per page</label>
                            <select  class=" bg-primary"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                                <?php
                                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >3 </option>';
                                else echo '<option  value="?ItemsPerPage=3"> 3 </option>';

                                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >4 </option>';
                                else  echo '<option  value="?ItemsPerPage=4"> 4 </option>';

                                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected > 5 </option>';
                                else echo '<option  value="?ItemsPerPage=5"> 5 </option>';

                                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected > 6 </option>';
                                else echo '<option  value="?ItemsPerPage=6">6 </option>';

                                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected > 10 </option>';
                                else echo '<option  value="?ItemsPerPage=10"> 10 </option>';

                                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected > 15 </option>';
                                else    echo '<option  value="?ItemsPerPage=15">15 </option>';
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="well" id="navbar">
                        <table class="table table-responsive">

                            <thead>
                            <tr class="bg-primary">
                                <th style="width:6%">SL.no</th>
                                <th style="width:6%">ID</th>
                                <th style="width:18%">BOOK TITLE</th>
                                <th style="width:27%">AUTHOR NAME</th>
                                <th style="width:42%">ACTION</th>
                            </tr>
                            </thead>


                          <?php   foreach($someData as $onedata){
                              echo "<tbody>";
                                echo "<tr>";
                                echo "<td>".$serial."</td>";
                                echo "<td>".$onedata->id."</td>";
                                echo "<td>".$onedata->booktitle."</td>";
                                echo "<td>".$onedata->author_name."</td>";
                               echo "<td>";
                                   //echo "<ul class='breadcrumb '>";
                              echo "<button class='btn btn-info' onclick=\"window.location.href='view.php?id=$onedata->id'\" >view</button>&nbsp;";
                              echo "<button class='btn btn-primary' onclick=\"window.location.href='edit.php?id=$onedata->id'\" >edit</button>&nbsp;";
                                      echo "<a href='trash.php?id=$onedata->id'><button class='btn btn-success'>Trash</button></a> ";
                                      echo "<a href='delete.php?id=$onedata->id'><button class='btn btn-danger'>delete</button> </a>";
                              echo "<a href='email.php?id=$onedata->id'><button class='btn btn-primary'>Email</button> </a>";
                              echo "</td>";

                              echo "</tr>";

                          $serial++;



                                 // echo  "</ul>";
                           echo "</tr>";
                              echo "</tbody>";
                          }
                          ?>


                        </table>
                    </div>
                    <nav aria-label="..." class="nav navbar-right">
                        <ul class="pagination">

                            <?php
                            $pre= $page-1;
                            $next=$page+1;
                            if($page>1)
                                echo "<li><a href='?Page=$pre'>". 'Previous' . '</a></li>';
                            for($i=1;$i<=$pages;$i++)
                            {
                                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                            }
                            if($page<$pages)
                                echo "<li><a href='?Page=$next'>". 'Next' . '</a></li>';
                            ?>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
        <div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3>Please enter Book Title and Author Name
                            <i class="fa fa-book fa-lg " aria-hidden="true"></i>
                        </h3>

                        <button type="button" class="close"
                                data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">

                        <form action="store.php" id="bookform" method="post" role="form">
                            <div class="form-group">
                                <label for="booktitle">Book Title</label>
                                <input type="text" class="form-control"name="booktitle"
                                       id="booktitle" placeholder="Enter booktitle"/>
                            </div>
                            <div class="form-group">
                                <label for="author_name">Author Name</label>
                                <input type="text" class="form-control"name="author_name"
                                       id="author_name" placeholder="Enter author_name"/>
                            </div>
                            <button type="submit" class="btn btn-info" ">Submit</button>


                        </form>
                    </div>
                    </div>
                </div>
            </div>



                </div>

    <!-- required for search, block 5 of 5 start -->
    <script>

        $(function() {
            var availableTags = [

                <?php
                echo $comma_separated_keywords;
                ?>
            ];
            // Filter function to search only from the beginning of the string
            $( "#searchID" ).autocomplete({
                source: function(request, response) {

                    var results = $.ui.autocomplete.filter(availableTags, request.term);

                    results = $.map(availableTags, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });

                    response(results.slice(0, 15));

                }
            });


            $( "#searchID" ).autocomplete({
                select: function(event, ui) {
                    $("#searchID").val(ui.item.label);
                    $("#searchForm").submit();
                }
            });


        });

    </script>
    <!-- required for search, block5 of 5 end -->


</body>

</html>