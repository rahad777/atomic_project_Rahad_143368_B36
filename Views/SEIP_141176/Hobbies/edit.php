<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Hobbies\Hobbies;
if(!isset($_SESSION))session_start();
$objHobbies=new Hobbies();
$objHobbies->setData($_GET);
$oneData=$objHobbies->view("obj");
$strvalue=$oneData->hobbie;
$str=explode(',',$strvalue);


?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../JS/create.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>

</head>
<body>

    <form class="navbar-form navbar-right">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div class="row">
        <div class="page-header" id="Mmessage">
            <?php echo Message::message(); ?>
        </div></div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li ><a href="../BookTitle/create.php">Book Title</a></li>
                    <li><a href="../Birthdate/create.php">Birthday</a></li>
                    <li><a href="../City/create.php">City</a></li>
                    <li><a href="../Email/create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li class="active"><a href="create.php">Hobbies</a></li>
                    <li><a href="../Profile_Picture/create.php">Profile Picture</a></li>
                    <li><a href="../Summary_Of_Organization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalNorm">
                            <span class="glyphicon glyphicon-plus">create new</span></button>

                        <div class="nav navbar-right">
                            <label >booklist per page</label>
                            <select class="bg-primary">
                                <option id="select">5</option>
                                <option id="select">10</option>
                                <option id="select">15</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="well " id="navbar">
                        <form action="update.php" id="hobbieform" method="post" role="form">
                            <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
                            <div class="form-group">
                                <label for="username">User Name</label>
                                <input type="text" class="form-control"name="username"
                                       id="user_name" value="<?php echo $oneData->username ?>"/>
                            </div>
                            <div class="form-group">
                                <label for="hobbies">Hobbies</label>

                                    <?php
                                    echo "<label class='checkbox-inline'>";
                                    if(in_array('reading', $str))
                                        echo '<input type="checkbox" name="hobbies[]" value="reading" checked>reading';
                                    else
                                        echo '<input type="checkbox" name="hobbies[]" value="reading">java';
                                    echo "</label>";
                                    echo "<label class='checkbox-inline'>";
                                    if(in_array('swimming', $str))
                                        echo '<input type="checkbox" name="hobbies[]" value="swimming" checked>swimming';
                                    else
                                        echo '<input type="checkbox" name="hobbies[]" value="swimming">swimming';
                                    echo "</label>";
                                    echo "<label class='checkbox-inline'>";
                                    if(in_array('watching movie', $str))
                                        echo '<input type="checkbox" name="hobbies[]" value="watching movie" checked>watching movie';
                                    else
                                        echo '<input type="checkbox" name="hobbies[]" value="watching movie">watching movie';
                                    echo "</label>";
                                    echo "<label class='checkbox-inline'>";
                                    if(in_array('singing', $str))
                                        echo '<input type="checkbox" name="hobbies[]" value="singing" checked>singing';
                                    else
                                        echo '<input type="checkbox" name="hobbies[]" value="singing">singigng';
                                    echo "</label>";
                                    ?>
                                   
                            </div>
                            <button type="submit" class="btn btn-info" >Submit</button>


                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>



</body>

</html>